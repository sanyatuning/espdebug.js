const net = require('net');

if (process.argv.length < 3) {
    console.log('Usage:');
    console.log('command <host or ip>');
    process.exit(1);
}

const options = {
    host: process.argv.pop(),
    port: 23
};
const socket = new net.Socket();

connect();

socket.on("connect", () => {
    console.log("Connected");
});

socket.on("timeout", () => {
    console.log("Timeout");
    socket.destroy();
});

socket.on("error", function(err) {
    //console.log("Error:", err.code);
});


socket.on("close", function() {
    console.log("closed");
    reconnect();
});

socket.on("data", function (data) {
    process.stdout.write(data);
});


function connect() {
    socket.connect(options);
    socket.setTimeout(10000);
}

function reconnect() {
    socket.end();
    setTimeout(() => {
        socket.end();
        console.log("Reconnecting...");
        connect();
    }, 500);
}
